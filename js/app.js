// Register service worker for SPA
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("/worker.js");
}

// Requesting permission for Notifications after clicking on the button
/*
const button = document.getElementById("notifications");
button.addEventListener("click", () => {
  Notification.requestPermission().then((result) => {
    if (result === "granted") {
      randomNotification();
    }
  });
});
*/

// Setting up random Notification
function randomNotification() {
  const title = "Hello there!";
  const body = "Hello there,\nYou visited my online CV recently. Thanks for stopping by!\nAre you still interested in any services I offer? Let me know!\nI wish you nice day!";
  const icon = "img/ad.png";
  const options = {body, icon};
  new Notification(title, options);
  const oneDay = 86400000;
  const twoDays = 172800000;
  setTimeout(randomNotification, Math.random() * (twoDays - oneDay) + oneDay);  // Send ad in 1-2 days
}

// Progressive loading images
let imagesToLoad = document.querySelectorAll("img[data-src]");
const loadImages = (image) => {
  image.setAttribute("src", image.getAttribute("data-src"));
  image.onload = () => {
    image.removeAttribute("data-src");
  };
};
if ("IntersectionObserver" in window) {
  const observer = new IntersectionObserver((items, observer) => {
    items.forEach((item) => {
      if (item.isIntersecting) {
        loadImages(item.target);
        observer.unobserve(item.target);
      }
    });
  });
  imagesToLoad.forEach((img) => {
    observer.observe(img);
  });
} else {
  imagesToLoad.forEach((img) => {
    loadImages(img);
  });
}

// Fix email in footer (spam protection)
window.onload = function(){
  var a = document.getElementById("spamfilter");
  a.href = a.href.replace("spamyuser", "email").replace("spamyhostname", "ricardo.band");
};

